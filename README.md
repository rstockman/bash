# Java Developer Sandbox Scripts
This repository contains scripts that I use to set up clean sandbox machines for java development activities. It assumes that is a fresh clean Ubuntu installation with nothing installed on it other than what comes packaged with the Ubuntu install. Note that these scripts intentionally do not include steps for installing any virtualbox guest additions.

There are some setup steps that need to be executed in order for the scripts in this repo to be properly utilized:

Run this command to install GIT.
```
sudo apt-get update
sudo apt-get install git -y
```
Then run this command to clone the repo to download the init scripts. Be sure to do this from your home directory and don't run as sudo.
```
cd ~
git clone https://bitbucket.org/rstockman/bash.git git/bash
```
If there is an existing ~/.bashrc file then you should run this command to rename it so we don't lose it. If not then you can skip this step. Be sure to use this exact naming convention because the bashrc script in the git repo will still try to execute ~/.bashrc_default
```
mv ~/.bashrc ~/.bashrc_default
```
Lastly, run these scripts to create symbolic links to the bash files that we maintain in the git repo. 
```
ln -fs ~/git/bash/bashrc ~/.bashrc
ln -fs ~/git/bash/bash_prompt ~/.bash_prompt
ln -fs ~/git/bash/bash_variables ~/.bash_variables
ln -fs ~/git/bash/bash_aliases ~/.bash_aliases
```
If this is a virtual machine that you are running on virtual box or vmware it may be a good place to create a snapshot of your machine here so that the above steps do not have to be repeated if creating additional VM's for different uses.

The remaining install scripts in this repo can be executed as needed. At a minimum you will probably want to execute the install_common_tools.sh script first. The other scripts are all optional to run as needed. All of these scripts should be executed as sudo.
```
cd ~/git/bash/
sudo ./install_common_tools.sh
```