


# --- control ls command output ---
alias ls='ls --color=auto'

# --- control grep command output ---
alias grep='grep --color=auto'

# --- create parent directories on demand ---
alias mkdir='mkdir -pv'

# --- set vim as default ---
alias vi='vim'
alias edit='vim'

# --- system commands ---
alias df='df -H'
alias du='du -ch'

# --- format data structures ---

# format json
alias json='python -mjson.tool'
#usage:
# cat some_unformatted_json.json |json
# curl http://somehost.com/result/1 -H accept:application/json |json

# format xml
alias xml='xmllint --format -'
#usage:
# cat some_unformatted_xml.xml |xml
# curl http://somehost.com/result/1 -H accept:application/xml |xml

# --- git commands ---
alias gitlog='git log --graph --pretty=tformat:"%Cred%h%Creset (%Cgreen%cd%Creset, %C(bold blue)%an%Creset): %s%C(yellow)%d%Creset" --date=format:"%Y-%m-%d %H:%M" --abbrev-commit'
