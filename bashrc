if [ -f ~/.bashrc_default ]; then
	. ~/.bashrc_default
fi

if [ -f ~/.bash_variables ]; then
	. ~/.bash_variables
fi

if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

if [ -f ~/.bash_prompt ]; then
	. ~/.bash_prompt
fi
