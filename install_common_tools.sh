#!/bin/sh

# get current user (and not just "root" if using sudo)
CURRENT_USER=`who | awk '{print $1}'`

# backup original files
if [ ! -f /etc/apt/sources.list_bak ]; then
	sudo cp /etc/apt/sources.list /etc/apt/sources.list_bak
	sudo echo "#$CURRENT_USER custom updates below" >> /etc/apt/sources.list
fi 

# update everything
sudo apt-get update
sudo apt-get upgrade -y

# install packages from ubuntu repository
sudo apt-get install openssh-server ssh wget unzip vim htop curl libxml2-utils openjdk-8-jdk scala ant maven gradle git subversion jmeter apache2 -y

