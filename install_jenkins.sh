#!/bin/sh

# NOTE: jenkins will initially start on port 8080. To change this edit this file:
# /etc/default/jenkins
# set HTTP_PORT to a different value
# then restart jenkins by executing:
# sudo service jenkins resta

# get current user (and not just "root" if using sudo)
CURRENT_USER=`who | awk '{print $1}'`

# backup original files
if [ ! -f /etc/apt/sources.list_bak ]; then
	sudo cp /etc/apt/sources.list /etc/apt/sources.list_bak
	sudo echo "#$CURRENT_USER custom updates below" >> /etc/apt/sources.list
fi 

# add jenkins to source list
if ! grep -q "custom jenkins install" /etc/apt/sources.list ; then
	wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
	sudo echo "#$CURRENT_USER custom jenkins install" >> /etc/apt/sources.list
	sudo echo "deb https://pkg.jenkins.io/debian-stable binary/" >> /etc/apt/sources.list
fi

# update everything
sudo apt-get update
sudo apt-get upgrade -y

# install jenkins
sudo apt-get install jenkins -y
